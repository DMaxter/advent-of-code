use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::Itertools;

fn add_distance(distances: &mut HashMap<String, HashMap<String, usize>>, line: &str) {
    let values: Vec<&str> = line.split(' ').collect();

    let source = values.first().unwrap();
    let dest = values.get(2).unwrap();
    let dist = values
        .get(4)
        .unwrap()
        .parse::<usize>()
        .expect("Invalid distance");

    match distances.get(*source) {
        None => {
            distances.insert((*source).to_owned(), HashMap::new());
        }
        Some(_) => (),
    };

    match distances.get(*dest) {
        None => {
            distances.insert((*dest).to_owned(), HashMap::new());
        }
        Some(_) => (),
    };

    distances
        .get_mut(*source)
        .unwrap()
        .insert((*dest).to_owned(), dist);
    distances
        .get_mut(*dest)
        .unwrap()
        .insert((*source).to_owned(), dist);
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut distances: HashMap<String, HashMap<String, usize>> = HashMap::new();

    for line in input.lines() {
        let line = line.expect("Invalid line");
        add_distance(&mut distances, &line[..]);
    }

    let mut min: usize = usize::MAX;

    for route in distances.keys().into_iter().permutations(distances.len()) {
        let len = route
            .iter()
            .tuple_windows()
            .map(|(src, dest)| distances.get(*src).unwrap().get(*dest).unwrap())
            .sum();

        if len < min {
            min = len;
        }
    }

    println!("{min}");

    Ok(())
}
