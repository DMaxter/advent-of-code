use std::fs::File;
use std::io::{BufRead, BufReader};

const MATRIX_ROWS: usize = 1000;
const MATRIX_COLS: usize = 1000;

type Matrix = [[bool; MATRIX_COLS]; MATRIX_ROWS];
type Coords = (usize, usize);

fn toggle(matrix: &mut Matrix, start: &Coords, end: &Coords) {
    for line in matrix.iter_mut().take(end.0 + 1).skip(start.0) {
        for light in line.iter_mut().take(end.1 + 1).skip(start.1) {
            *light = !*light
        }
    }
}

fn turn(matrix: &mut Matrix, start: &Coords, end: &Coords, value: bool) {
    for line in matrix.iter_mut().take(end.0 + 1).skip(start.0) {
        for light in line.iter_mut().take(end.1 + 1).skip(start.1) {
            *light = value
        }
    }
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut matrix: Matrix = [[false; MATRIX_COLS]; MATRIX_ROWS];

    for line in input.lines() {
        let line = line.unwrap();
        let split = line.split(" through ").collect::<Vec<&str>>();
        let verb = split[0].split(' ').collect::<Vec<&str>>();
        let left = verb.last().unwrap().split(',').collect::<Vec<&str>>();
        let right = split[1].split(',').collect::<Vec<&str>>();

        let end: Coords = (right[0].parse().unwrap(), right[1].parse().unwrap());
        let start: Coords = (left[0].parse().unwrap(), left[1].parse().unwrap());

        match verb[0] {
            "toggle" => toggle(&mut matrix, &start, &end),
            "turn" => match verb[1] {
                "on" => turn(&mut matrix, &start, &end, true),
                "off" => turn(&mut matrix, &start, &end, false),
                _ => unreachable!("Unknown turn command"),
            },
            _ => unreachable!("Unknown command"),
        }
    }

    println!(
        "{}",
        matrix
            .iter()
            .map(|row| { row.iter().filter(|x| **x).count() })
            .sum::<usize>()
    );

    Ok(())
}
