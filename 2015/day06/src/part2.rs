use std::cmp::max;
use std::fs::File;
use std::io::{BufRead, BufReader};

const MATRIX_ROWS: usize = 1000;
const MATRIX_COLS: usize = 1000;

type Matrix = [[isize; MATRIX_COLS]; MATRIX_ROWS];
type Coords = (usize, usize);

fn toggle(matrix: &mut Matrix, start: &Coords, end: &Coords) {
    for line in matrix.iter_mut().take(end.0 + 1).skip(start.0) {
        for light in line.iter_mut().take(end.1 + 1).skip(start.1) {
            *light += 2
        }
    }
}

fn turn_on(matrix: &mut Matrix, start: &Coords, end: &Coords) {
    for line in matrix.iter_mut().take(end.0 + 1).skip(start.0) {
        for light in line.iter_mut().take(end.1 + 1).skip(start.1) {
            *light += 1
        }
    }
}

fn turn_off(matrix: &mut Matrix, start: &Coords, end: &Coords) {
    for line in matrix.iter_mut().take(end.0 + 1).skip(start.0) {
        for light in line.iter_mut().take(end.1 + 1).skip(start.1) {
            *light = max(*light - 1, 0)
        }
    }
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut matrix: Matrix = [[0; MATRIX_COLS]; MATRIX_ROWS];

    for line in input.lines() {
        let line = line.unwrap();
        let split = line.split(" through ").collect::<Vec<&str>>();
        let verb = split[0].split(' ').collect::<Vec<&str>>();
        let left = verb.last().unwrap().split(',').collect::<Vec<&str>>();
        let right = split[1].split(',').collect::<Vec<&str>>();

        let end: Coords = (right[0].parse().unwrap(), right[1].parse().unwrap());
        let start: Coords = (left[0].parse().unwrap(), left[1].parse().unwrap());

        match verb[0] {
            "toggle" => toggle(&mut matrix, &start, &end),
            "turn" => match verb[1] {
                "on" => turn_on(&mut matrix, &start, &end),
                "off" => turn_off(&mut matrix, &start, &end),
                _ => unreachable!("Unknown turn command"),
            },
            _ => unreachable!("Unknown command"),
        }
    }

    println!(
        "{}",
        matrix
            .iter()
            .map(|row| { row.iter().sum::<isize>() })
            .sum::<isize>()
    );

    Ok(())
}
