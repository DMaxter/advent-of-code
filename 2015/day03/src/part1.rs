use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use day03::Point;

fn move_pos(position: Rc<Point>, movement: char) -> Rc<Point> {
    Rc::new(match movement {
        '>' => Point(position.0 + 1, position.1),
        '<' => Point(position.0 - 1, position.1),
        '^' => Point(position.0, position.1 + 1),
        'v' => Point(position.0, position.1 - 1),
        _ => unreachable!("Invalid character found"),
    })
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut points: HashSet<Rc<Point>> = HashSet::new();
    let mut current_position = Rc::new(Point(0, 0));
    points.insert(Rc::clone(&current_position));

    for movement in input
        .lines()
        .flat_map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
    {
        current_position = move_pos(current_position, movement);
        points.insert(Rc::clone(&current_position));
    }

    println!("{}", points.len());

    Ok(())
}
