use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::rc::Rc;

use day03::Point;

fn move_pos(position: Rc<Point>, movement: char) -> Rc<Point> {
    Rc::new(match movement {
        '>' => Point(position.0 + 1, position.1),
        '<' => Point(position.0 - 1, position.1),
        '^' => Point(position.0, position.1 + 1),
        'v' => Point(position.0, position.1 - 1),
        _ => unreachable!("Invalid character found"),
    })
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut points: HashSet<Rc<Point>> = HashSet::new();
    let mut current_position_santa = Rc::new(Point(0, 0));
    let mut current_position_robot = Rc::clone(&current_position_santa);
    points.insert(Rc::clone(&current_position_santa));

    for (num, movement) in input
        .lines()
        .flat_map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
        .enumerate()
    {
        if num % 2 == 0 {
            current_position_santa = move_pos(current_position_santa, movement);
            points.insert(Rc::clone(&current_position_santa));
        } else {
            current_position_robot = move_pos(current_position_robot, movement);
            points.insert(Rc::clone(&current_position_robot));
        }
    }

    println!("{}", points.len());

    Ok(())
}
