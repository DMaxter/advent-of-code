use std::fs::read_to_string;

fn parse_input(input: &str) -> String {
    let mut res = String::new();

    let mut count: usize = 0;
    let mut prev: Option<char> = None;

    for c in input.chars() {
        if let Some(p) = prev {
            if p == c {
                count += 1;
            } else {
                res.push_str(&format!("{count}{p}")[..]);
                prev = Some(c);
                count = 1;
            }
        } else {
            prev = Some(c);
            count = 1;
        }
    }

    res.push_str(&format!("{count}{}", prev.unwrap())[..]);

    res
}

fn main() -> std::io::Result<()> {
    let mut input = read_to_string("input")?;

    for _ in 1..51 {
        input = parse_input(&input);
    }

    println!("{}", input.len());

    Ok(())
}
