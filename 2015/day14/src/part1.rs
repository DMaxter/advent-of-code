use std::cmp::min;
use std::fs::File;
use std::io::{BufRead, BufReader};

const SECONDS: usize = 2503;

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut max: usize = usize::MIN;

    for line in input.lines() {
        let line = line.unwrap();
        let values: Vec<&str> = line.split(' ').collect();
        let velocity = values[3].parse::<usize>().expect("Invalid velocity");
        let fly_time = values[6].parse::<usize>().expect("Invalid fly time");
        let rest_time = values[13].parse::<usize>().expect("Invalid rest time");
        let full_cycles = SECONDS / (fly_time + rest_time);
        let remaining_seconds = SECONDS % (fly_time + rest_time);

        let distance = (full_cycles * fly_time + min(remaining_seconds, fly_time)) * velocity;

        if distance > max {
            max = distance;
        }
    }

    println!("{max}");

    Ok(())
}
