use std::cmp::Ordering;
use std::fs::File;
use std::io::{BufRead, BufReader};

const SECONDS: usize = 2503;

#[derive(Debug)]
struct Reindeer {
    pub velocity: usize,
    pub fly_time: usize,
    pub cycle_time: usize,
    pub points: usize,
    pub cur_dist: usize,
}

impl Reindeer {
    pub fn new(velocity: usize, fly_time: usize, rest_time: usize) -> Self {
        Reindeer {
            velocity,
            fly_time,
            cycle_time: fly_time + rest_time,
            points: 0,
            cur_dist: 0,
        }
    }

    pub fn fly(&mut self, time: usize) {
        let cycle_seconds = (time - 1) % (self.cycle_time);

        if cycle_seconds < self.fly_time {
            self.cur_dist += self.velocity;
        }
    }

    pub fn score(&mut self) {
        self.points += 1;
    }
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut reindeers: Vec<Reindeer> = Vec::new();

    for line in input.lines() {
        let line = line.unwrap();
        let values: Vec<&str> = line.split(' ').collect();
        reindeers.push(Reindeer::new(
            values[3].parse::<usize>().expect("Invalid velocity"),
            values[6].parse::<usize>().expect("Invalid fly time"),
            values[13].parse::<usize>().expect("Invalid rest time"),
        ));
    }

    for time in 1..=SECONDS {
        reindeers.iter_mut().for_each(|r| r.fly(time));
        let max_dist = reindeers
            .iter()
            .max_by(|a, b| {
                if a.cur_dist < b.cur_dist {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            })
            .map(|r| r.cur_dist)
            .unwrap();

        reindeers
            .iter_mut()
            .filter(|r| r.cur_dist == max_dist)
            .for_each(|r| r.score());
    }

    println!(
        "{}",
        reindeers
            .iter()
            .max_by(|a, b| {
                if a.points < b.points {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            })
            .map(|r| r.points)
            .unwrap()
    );

    Ok(())
}
