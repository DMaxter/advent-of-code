use std::fs::read_to_string;

use itertools::Itertools;

fn increase_letter(c: char) -> char {
    let res: char = (((c as u8 - b'a' + 1) % (b'z' - b'a' + 1)) + b'a') as char;

    match res {
        'i' | 'l' | 'o' => ((res as u8) + 1) as char,
        _ => res,
    }
}

fn increasing_three_letters(password: &str) -> bool {
    password
        .chars()
        .tuple_windows::<(_, _, _)>()
        .filter(|(c1, c2, c3)| (*c3 as i8 - *c2 as i8) == 1 && (*c2 as i8 - *c1 as i8) == 1)
        .count()
        > 0
}

fn two_pairs(password: &str) -> bool {
    password
        .chars()
        .tuple_windows()
        .filter_map(|(c1, c2)| if c1 == c2 { Some(c1) } else { None })
        .dedup()
        .count()
        > 1
}

fn valid_password(password: &str) -> bool {
    increasing_three_letters(password) && two_pairs(password)
}

fn increase_password(password: &mut String) {
    let mut chars = password.chars().collect::<Vec<char>>();

    let mut overflow = true;

    chars.iter_mut().rev().for_each(|c| {
        *c = if overflow {
            let tmp = increase_letter(*c);

            overflow = *c > tmp;

            tmp
        } else {
            *c
        }
    });

    *password = chars.into_iter().collect()
}

fn main() -> std::io::Result<()> {
    let mut input = read_to_string("input")?;

    for _ in 0..2 {
        increase_password(&mut input);

        while !valid_password(&input) {
            increase_password(&mut input);
        }
    }

    println!("{input}");

    Ok(())
}
