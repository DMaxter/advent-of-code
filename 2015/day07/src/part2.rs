use std::collections::HashMap;
use std::fs::read_to_string;

use day07::{parse_line, Operation, Value};

fn get_value(wire: &str, wires: &HashMap<String, Operation>) -> u16 {
    let mut queue: Vec<(&str, &Operation)> = Vec::new();
    let mut values: HashMap<&str, u16> = HashMap::new();

    queue.push((wire, wires.get(wire).expect("Unknown result cable")));

    while !queue.is_empty() {
        let (cur_wire, cur_op) = queue.last().cloned().unwrap();

        match cur_op {
            Operation::Assign(num) => {
                match num {
                    Value::Literal(val) => {
                        values.insert(cur_wire, *val);
                        queue.pop();
                    }
                    Value::Label(label) => {
                        if let Some(val) = values.get(&label[..]) {
                            values.insert(cur_wire, *val);
                            queue.pop();
                        } else {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }
                    }
                };
            }
            Operation::AND((left, right)) => {
                let left_value = match left {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                let right_value = match right {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                if let Some(left_value) = left_value {
                    if let Some(right_value) = right_value {
                        values.insert(cur_wire, left_value & right_value);
                        queue.pop();
                    }
                }
            }
            Operation::OR((left, right)) => {
                let left_value = match left {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                let right_value = match right {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                if let Some(left_value) = left_value {
                    if let Some(right_value) = right_value {
                        values.insert(cur_wire, left_value | right_value);
                        queue.pop();
                    }
                }
            }
            Operation::LSHIFT((left, right)) => {
                let left_value = match left {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                let right_value = match right {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                if let Some(left_value) = left_value {
                    if let Some(right_value) = right_value {
                        values.insert(cur_wire, left_value << right_value);
                        queue.pop();
                    }
                }
            }
            Operation::RSHIFT((left, right)) => {
                let left_value = match left {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                let right_value = match right {
                    Value::Literal(val) => Some(val),
                    Value::Label(label) => {
                        let val = values.get(&label[..]);

                        if val.is_none() {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }

                        val
                    }
                };

                if let Some(left_value) = left_value {
                    if let Some(right_value) = right_value {
                        values.insert(cur_wire, left_value >> right_value);
                        queue.pop();
                    }
                }
            }
            Operation::NOT(num) => {
                match num {
                    Value::Literal(val) => {
                        values.insert(cur_wire, *val);
                    }
                    Value::Label(label) => {
                        if let Some(val) = values.get(&label[..]) {
                            values.insert(cur_wire, !*val);
                            queue.pop();
                        } else {
                            queue.push((&label[..], wires.get(label).expect("Unknown cable")));
                        }
                    }
                };
            }
        }
    }

    values
        .get(wire)
        .expect("Value for wire not found")
        .to_owned()
}

fn main() -> std::io::Result<()> {
    let mut wires: HashMap<String, Operation> = HashMap::new();

    let input = read_to_string("input")?;

    for line in input.lines() {
        let (_, (wire, operation)) = parse_line(line).expect("Invalid line");

        wires.insert(wire, operation);
    }

    let b = get_value("a", &wires);

    wires.insert("b".to_owned(), Operation::Assign(Value::Literal(b)));

    println!("{}", get_value("a", &wires));

    Ok(())
}
