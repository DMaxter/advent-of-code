use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alpha1, digit1, multispace1};
use nom::sequence::{preceded, tuple};
use nom::IResult;

#[derive(Clone, Debug)]
pub enum Operation {
    AND((Value, Value)),
    NOT(Value),
    OR((Value, Value)),
    LSHIFT((Value, Value)),
    RSHIFT((Value, Value)),
    Assign(Value),
}

#[derive(Clone, Debug)]
pub enum Value {
    Label(String),
    Literal(u16),
}

fn parse_wire(input: &str) -> IResult<&str, Value> {
    let (input, wire) = alpha1(input)?;

    Ok((input, Value::Label(wire.to_owned())))
}

fn parse_number(input: &str) -> IResult<&str, Value> {
    let (input, numstr) = digit1(input)?;

    let num = numstr.parse().expect("Invalid number");

    Ok((input, Value::Literal(num)))
}

fn parse_value(input: &str) -> IResult<&str, Value> {
    alt((parse_number, parse_wire))(input)
}

fn parse_and(input: &str) -> IResult<&str, Operation> {
    let (input, (left, right)) = tuple((
        parse_value,
        preceded(
            preceded(multispace1, tag("AND")),
            preceded(multispace1, parse_value),
        ),
    ))(input)?;

    Ok((input, Operation::AND((left, right))))
}

fn parse_or(input: &str) -> IResult<&str, Operation> {
    let (input, (left, right)) = tuple((
        parse_value,
        preceded(
            preceded(multispace1, tag("OR")),
            preceded(multispace1, parse_value),
        ),
    ))(input)?;

    Ok((input, Operation::OR((left, right))))
}

fn parse_not(input: &str) -> IResult<&str, Operation> {
    let (input, operand) = preceded(tag("NOT"), preceded(multispace1, parse_value))(input)?;

    Ok((input, Operation::NOT(operand)))
}

fn parse_lshift(input: &str) -> IResult<&str, Operation> {
    let (input, (left, right)) = tuple((
        parse_value,
        preceded(
            preceded(multispace1, tag("LSHIFT")),
            preceded(multispace1, parse_number),
        ),
    ))(input)?;

    Ok((input, Operation::LSHIFT((left, right))))
}

fn parse_rshift(input: &str) -> IResult<&str, Operation> {
    let (input, (left, right)) = tuple((
        parse_value,
        preceded(
            preceded(multispace1, tag("RSHIFT")),
            preceded(multispace1, parse_number),
        ),
    ))(input)?;

    Ok((input, Operation::RSHIFT((left, right))))
}

fn parse_assignment(input: &str) -> IResult<&str, Operation> {
    let (input, val) = parse_value(input)?;

    Ok((input, Operation::Assign(val)))
}

fn parse_operation(input: &str) -> IResult<&str, Operation> {
    alt((
        parse_and,
        parse_or,
        parse_not,
        parse_lshift,
        parse_rshift,
        parse_assignment,
    ))(input)
}

pub fn parse_line(input: &str) -> IResult<&str, (String, Operation)> {
    let (input, (operation, wire)) =
        tuple((parse_operation, preceded(tag(" -> "), parse_wire)))(input)?;

    Ok((
        input,
        (
            match wire {
                Value::Label(label) => label,
                _ => panic!("Invalid wire"),
            },
            operation,
        ),
    ))
}
