use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| -> usize {
                let measurements: Vec<usize> = l
                    .unwrap()
                    .split('x')
                    .map(|m| m.parse::<usize>().unwrap())
                    .collect();

                let areas = vec![
                    measurements[0] * measurements[1],
                    measurements[0] * measurements[2],
                    measurements[1] * measurements[2],
                ];

                2 * (areas.iter().sum::<usize>()) + areas.iter().min().unwrap()
            })
            .sum::<usize>()
    );

    Ok(())
}
