use std::collections::VecDeque;
use std::fs::read_to_string;

use json::JsonValue;

fn main() -> std::io::Result<()> {
    let input = read_to_string("input")?;

    let mut objects = VecDeque::new();
    objects.push_back(json::parse(&input[..]).expect("Invalid JSON!"));

    let mut sum: f64 = 0.0;

    while let Some(object) = objects.pop_front() {
        match object {
            JsonValue::Array(array) => array.iter().for_each(|e| objects.push_front(e.clone())),
            JsonValue::Number(num) => sum += f64::from(num),
            JsonValue::Object(obj) => obj.iter().for_each(|e| objects.push_front(e.1.clone())),
            _ => (),
        }
    }

    println!("{sum}");

    Ok(())
}
