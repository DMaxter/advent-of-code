use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut floor = 0;

    for c in input
        .lines()
        .flat_map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
        .enumerate()
    {
        floor += match c.1 {
            '(' => 1,
            ')' => -1,
            _ => unreachable!("Invalid character found"),
        };

        if floor == -1 {
            println!("{}", c.0 + 1);

            break;
        }
    }

    Ok(())
}
