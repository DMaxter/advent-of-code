use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .flat_map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
            .map(|c: char| -> isize {
                match c {
                    '(' => 1,
                    ')' => -1,
                    _ => unreachable!("Invalid character found"),
                }
            })
            .sum::<isize>()
    );

    Ok(())
}
