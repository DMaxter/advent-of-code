use std::fs::File;
use std::io::{BufRead, BufReader};

use fancy_regex::Regex;

use lazy_static::lazy_static;

fn match_pair_letters(line: &str) -> bool {
    lazy_static! {
        static ref PATTERN: Regex = Regex::new(r"(\w\w).*\1").unwrap();
    }

    PATTERN.is_match(line).unwrap()
}

fn match_repeated_letter_separated(line: &str) -> bool {
    lazy_static! {
        static ref PATTERN: Regex = Regex::new(r"(\w)\w\1").unwrap();
    }

    PATTERN.is_match(line).unwrap()
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .filter(|u| {
                let line = u.as_ref().unwrap();

                match_pair_letters(line) && match_repeated_letter_separated(line)
            })
            .count()
    );

    Ok(())
}
