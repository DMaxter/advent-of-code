use std::fs::File;
use std::io::{BufRead, BufReader};

use fancy_regex::Regex;

use lazy_static::lazy_static;

fn match_more_than_three_vowels(line: &str) -> bool {
    lazy_static! {
        static ref PATTERN: Regex = Regex::new(r"([aeiou].*){3,}").unwrap();
    }

    PATTERN.is_match(line).unwrap()
}

fn match_two_consecutive_letters(line: &str) -> bool {
    lazy_static! {
        static ref PATTERN: Regex = Regex::new(r"(\w)\1").unwrap();
    }

    PATTERN.is_match(line).unwrap()
}

fn match_strings(line: &str) -> bool {
    lazy_static! {
        static ref PATTERN: Regex = Regex::new(r"(ab|cd|pq|xy)").unwrap();
    }

    PATTERN.is_match(line).unwrap()
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .filter(|u| {
                let line = u.as_ref().unwrap();

                match_more_than_three_vowels(line)
                    && match_two_consecutive_letters(line)
                    && !match_strings(line)
            })
            .count()
    );

    Ok(())
}
