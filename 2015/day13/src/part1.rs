use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

use itertools::Itertools;

fn add_preference(preferences: &mut HashMap<String, HashMap<String, isize>>, line: &str) {
    let values: Vec<&str> = line.split(' ').collect();

    let p1 = values.first().unwrap();
    let p2 = values.get(10).unwrap();
    let p2 = &p2[..p2.len() - 1];
    let sign = values.get(2).unwrap();
    let happiness = values.get(3).unwrap();

    match preferences.get(*p1) {
        None => {
            preferences.insert((*p1).to_owned(), HashMap::new());
        }
        Some(_) => (),
    }

    preferences.get_mut(*p1).unwrap().insert(
        (*p2).to_owned(),
        if sign == &"gain" {
            happiness.parse::<isize>().expect("Invalid happiness value")
        } else {
            -happiness.parse::<isize>().expect("Invalid happiness value")
        },
    );
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut preferences: HashMap<String, HashMap<String, isize>> = HashMap::new();

    for line in input.lines() {
        let line = line.expect("Invalid line");
        add_preference(&mut preferences, &line[..]);
    }

    let mut max: isize = isize::MIN;

    for mut combination in preferences
        .keys()
        .into_iter()
        .permutations(preferences.len())
    {
        combination.push(combination.first().unwrap());
        let happiness = combination
            .iter()
            .tuple_windows()
            .map(|(p1, p2)| {
                preferences.get(*p1).unwrap().get(*p2).unwrap()
                    + preferences.get(*p2).unwrap().get(*p1).unwrap()
            })
            .sum();

        if happiness > max {
            max = happiness;
        }
    }

    println!("{max}");

    Ok(())
}
