use std::fs::read_to_string;

fn main() -> std::io::Result<()> {
    let input = read_to_string("input")?;

    let mut num: usize = 0;

    loop {
        let digest = format!("{:x}", md5::compute(input.clone() + &num.to_string()));

        if digest.starts_with("000000") {
            println!("{num}");
            break;
        }

        num += 1;
    }

    Ok(())
}
