use std::fs::read_to_string;

use day08::parse_line;

fn main() -> std::io::Result<()> {
    let input = read_to_string("input")?;

    let mut total_chars: usize = 0;
    let mut actual_chars: usize = 0;

    for line in input.lines() {
        let (_, (count, _)) = parse_line(line).expect("Invalid line");

        total_chars += line.len();
        actual_chars += count;
    }

    println!("{}", total_chars - actual_chars);

    Ok(())
}
