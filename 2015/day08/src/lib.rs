use nom::branch::alt;
use nom::bytes::complete::{tag, take_while_m_n};
use nom::combinator::map_res;
use nom::error::{Error, ParseError};
use nom::multi::many0;
use nom::sequence::{preceded, terminated, tuple};
use nom::Err;
use nom::IResult;

fn parse_quote(input: &str) -> IResult<&str, usize> {
    let (input, _) = tag("\"")(input)?;

    Ok((input, 2))
}

fn parse_backslash(input: &str) -> IResult<&str, usize> {
    let (input, _) = tag("\\")(input)?;

    Ok((input, 2))
}

fn parse_none(_: &str) -> Result<usize, ()> {
    Ok(0)
}

fn is_hex_digit(chr: char) -> bool {
    chr.is_ascii_hexdigit()
}

fn parse_hexdigits(input: &str) -> IResult<&str, usize> {
    let (input, _) = map_res(take_while_m_n(2, 2, is_hex_digit), parse_none)(input)?;

    Ok((input, 2))
}

fn parse_hexcode(input: &str) -> IResult<&str, usize> {
    let (input, _) = tuple((tag("x"), parse_hexdigits))(input)?;

    Ok((input, 3))
}

fn parse_escape(input: &str) -> IResult<&str, usize> {
    let (input, (_, count)) = tuple((
        tag("\\"),
        alt((parse_quote, parse_hexcode, parse_backslash)),
    ))(input)?;

    // Escape the current backslash plus another to escape it
    Ok((input, count + 2))
}

fn parse_regular_char(input: &str) -> IResult<&str, usize> {
    let mut it = input.chars();

    let chr = it.next();

    match chr {
        Some(chr) => {
            if chr != '"' {
                Ok((&input[1..], 1))
            } else {
                Err(Err::Error(Error::from_char(&input[0..1], '"')))
            }
        }
        None => Err(Err::Error(Error::from_char("", '"'))),
    }
}

fn parse_char(input: &str) -> IResult<&str, usize> {
    alt((parse_escape, parse_regular_char))(input)
}

pub fn parse_line(input: &str) -> IResult<&str, (usize, usize)> {
    let (input, individual_chars) =
        preceded(parse_quote, terminated(many0(parse_char), parse_quote))(input)?;

    let sum: usize = individual_chars.iter().sum();

    // Escape the 2 quotes and add 2 more quotes
    Ok((input, (individual_chars.len(), sum + 6)))
}
