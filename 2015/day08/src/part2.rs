use std::fs::read_to_string;

use day08::parse_line;

fn main() -> std::io::Result<()> {
    let input = read_to_string("input")?;

    let mut total_chars: usize = 0;
    let mut escape_chars: usize = 0;

    for line in input.lines() {
        let (_, (_, count)) = parse_line(line).expect("Invalid line");

        total_chars += line.len();
        escape_chars += count;
    }

    println!("{}", escape_chars - total_chars);

    Ok(())
}
