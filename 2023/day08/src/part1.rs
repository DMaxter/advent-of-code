use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines = input.lines();

    let line = lines.next().unwrap()?;
    let instructions = line.chars().cycle();

    let directions = lines
        .skip(1)
        .map(|l| {
            let l = l.unwrap();

            let values: Vec<&str> = l.split(" = ").collect();

            let nodes: Vec<&str> = values[1].split(", ").collect();

            (
                String::from(values[0]),
                [String::from(&nodes[0][1..]), String::from(&nodes[1][..3])],
            )
        })
        .fold(HashMap::new(), |mut map, (k, v)| {
            map.insert(k, v);

            map
        });

    let mut current = "AAA";
    for (n, instruction) in instructions.enumerate() {
        current = &directions.get(current).unwrap()[match instruction {
            'L' => 0,
            'R' => 1,
            _ => unreachable!(),
        }];

        if current == "ZZZ" {
            println!("{}", n + 1);
            break;
        }
    }

    Ok(())
}
