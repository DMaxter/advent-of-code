use num::integer::lcm;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines = input.lines();

    let line = lines.next().unwrap()?;
    let instructions = line.chars().cycle();

    let mut start_nodes: Vec<String> = Vec::new();

    let directions = lines
        .skip(1)
        .map(|l| {
            let l = l.unwrap();

            let values: Vec<&str> = l.split(" = ").collect();

            let nodes: Vec<&str> = values[1].split(", ").collect();

            let key = String::from(values[0]);

            if key.chars().nth(2).unwrap() == 'A' {
                start_nodes.push(key.clone());
            }

            (
                key,
                [String::from(&nodes[0][1..]), String::from(&nodes[1][..3])],
            )
        })
        .fold(HashMap::new(), |mut map, (k, v)| {
            map.insert(k, v);

            map
        });

    let get_steps = |start: &String| -> usize {
        let mut current = start;
        for (n, instruction) in instructions.clone().enumerate() {
            current = &directions.get(current).unwrap()[match instruction {
                'L' => 0,
                'R' => 1,
                _ => unreachable!(),
            }];

            if current.chars().nth(2).unwrap() == 'Z' {
                return n + 1;
            }
        }
        unreachable!()
    };

    println!("{}", start_nodes.iter().map(get_steps).reduce(lcm).unwrap());

    Ok(())
}
