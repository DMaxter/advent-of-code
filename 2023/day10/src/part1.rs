use day10::{match_pipe, Direction, Pipe, PipeType};
use phf::phf_map;
use std::{
    cell::RefCell,
    fs::File,
    io::{BufRead, BufReader},
    rc::Rc,
};

static VALID_PIPES: phf::Map<u8, [PipeType; 4]> = phf_map! {
    // North
    1u8 => [PipeType::SW, PipeType::SE, PipeType::NS, PipeType::All],
    // West
    2u8 => [PipeType::NE, PipeType::SE, PipeType::WE, PipeType::All]
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut maze: Vec<Vec<Rc<RefCell<Pipe>>>> = Vec::new();

    let mut start: (usize, usize) = (0, 0);

    for (i, line) in input.lines().enumerate() {
        let line = line.unwrap();

        let mut current: Vec<Rc<RefCell<Pipe>>> = Vec::new();

        for (j, c) in line.chars().enumerate() {
            let kind = match c {
                'L' => PipeType::NE,
                'J' => PipeType::NW,
                '7' => PipeType::SW,
                'F' => PipeType::SE,
                '|' => PipeType::NS,
                '-' => PipeType::WE,
                'S' => {
                    start = (i, j);
                    PipeType::All
                }
                '.' => {
                    current.push(Rc::new(RefCell::new(Pipe {
                        kind: PipeType::None,
                        distance: None,
                        neighbours: Vec::new(),
                    })));
                    continue;
                }
                _ => continue,
            };

            let mut neighbours = Vec::new();

            match kind {
                PipeType::NE if i > 0 => {
                    if match_pipe(
                        Rc::clone(&maze[i - 1][j]),
                        VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                }
                PipeType::NW => {
                    if i > 0
                        && match_pipe(
                            Rc::clone(&maze[i - 1][j]),
                            VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                    if j > 0
                        && match_pipe(
                            Rc::clone(&current[j - 1]),
                            VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::SW if j > 0 => {
                    if match_pipe(
                        Rc::clone(&current[j - 1]),
                        VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::NS if i > 0 => {
                    if match_pipe(
                        Rc::clone(&maze[i - 1][j]),
                        VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                }
                PipeType::WE if j > 0 => {
                    if match_pipe(
                        Rc::clone(&current[j - 1]),
                        VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::All => {
                    if i > 0
                        && match_pipe(
                            Rc::clone(&maze[i - 1][j]),
                            VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                    if j > 0
                        && match_pipe(
                            Rc::clone(&current[j - 1]),
                            VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                _ => (),
            };

            let pipe = Rc::new(RefCell::new(Pipe {
                kind,
                neighbours,
                distance: None,
            }));

            pipe.borrow()
                .neighbours
                .iter()
                .for_each(|p| p.borrow_mut().neighbours.push(Rc::clone(&pipe)));

            current.push(pipe);
        }
        maze.push(current);
    }

    let mut to_visit = Vec::from([Rc::clone(&maze[start.0][start.1])]);
    let mut distance = 0;

    while !to_visit.is_empty() {
        let mut new_to_visit = Vec::new();
        for p in to_visit {
            p.borrow_mut().distance = Some(distance);

            let pipe = p.borrow();

            pipe.neighbours
                .iter()
                .filter(|p| p.borrow().distance.is_none())
                .for_each(|p| new_to_visit.push(Rc::clone(p)));
        }
        to_visit = new_to_visit;
        distance += 1;
    }

    println!("{}", distance - 1);

    Ok(())
}
