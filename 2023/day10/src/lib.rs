use std::{cell::RefCell, rc::Rc};

#[repr(u8)]
pub enum Direction {
    North = 1,
    West = 2,
    East = 3,
    South = 4,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PipeType {
    All,
    NW,
    NE,
    SW,
    SE,
    NS,
    WE,
    None,
}

#[derive(Debug)]
pub struct Pipe {
    pub kind: PipeType,
    pub distance: Option<usize>,
    pub neighbours: Vec<Rc<RefCell<Pipe>>>,
}

pub fn match_pipe(pipe: Rc<RefCell<Pipe>>, possible: &[PipeType]) -> bool {
    possible.contains(&pipe.borrow().kind)
}
