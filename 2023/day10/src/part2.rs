use day10::{match_pipe, Direction, Pipe, PipeType};
use phf::phf_map;
use std::{
    cell::RefCell,
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
    rc::Rc,
};

static VALID_PIPES: phf::Map<u8, [PipeType; 4]> = phf_map! {
    // North
    1u8 => [PipeType::SW, PipeType::SE, PipeType::NS, PipeType::All],
    // West
    2u8 => [PipeType::NE, PipeType::SE, PipeType::WE, PipeType::All],
    // East
    3u8 => [PipeType::NW, PipeType::SW, PipeType::WE, PipeType::All],
    // South
    4u8 => [PipeType::NW, PipeType::NE, PipeType::NS, PipeType::All],
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut maze: Vec<Vec<Rc<RefCell<Pipe>>>> = Vec::new();

    let mut start: (usize, usize) = (0, 0);

    for (i, line) in input.lines().enumerate() {
        let line = line.unwrap();

        let mut current: Vec<Rc<RefCell<Pipe>>> = Vec::new();

        for (j, c) in line.chars().enumerate() {
            let kind = match c {
                'L' => PipeType::NE,
                'J' => PipeType::NW,
                '7' => PipeType::SW,
                'F' => PipeType::SE,
                '|' => PipeType::NS,
                '-' => PipeType::WE,
                'S' => {
                    start = (i, j);
                    PipeType::All
                }
                '.' => {
                    current.push(Rc::new(RefCell::new(Pipe {
                        kind: PipeType::None,
                        distance: None,
                        neighbours: Vec::new(),
                    })));
                    continue;
                }
                _ => continue,
            };

            let mut neighbours = Vec::new();

            match kind {
                PipeType::NE if i > 0 => {
                    if match_pipe(
                        Rc::clone(&maze[i - 1][j]),
                        VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                }
                PipeType::NW => {
                    if i > 0
                        && match_pipe(
                            Rc::clone(&maze[i - 1][j]),
                            VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                    if j > 0
                        && match_pipe(
                            Rc::clone(&current[j - 1]),
                            VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::SW if j > 0 => {
                    if match_pipe(
                        Rc::clone(&current[j - 1]),
                        VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::NS if i > 0 => {
                    if match_pipe(
                        Rc::clone(&maze[i - 1][j]),
                        VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                }
                PipeType::WE if j > 0 => {
                    if match_pipe(
                        Rc::clone(&current[j - 1]),
                        VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                    ) {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                PipeType::All => {
                    if i > 0
                        && match_pipe(
                            Rc::clone(&maze[i - 1][j]),
                            VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&maze[i - 1][j]));
                    }
                    if j > 0
                        && match_pipe(
                            Rc::clone(&current[j - 1]),
                            VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
                        )
                    {
                        neighbours.push(Rc::clone(&current[j - 1]));
                    }
                }
                _ => (),
            };

            let pipe = Rc::new(RefCell::new(Pipe {
                kind,
                neighbours,
                distance: None,
            }));

            pipe.borrow()
                .neighbours
                .iter()
                .for_each(|p| p.borrow_mut().neighbours.push(Rc::clone(&pipe)));

            current.push(pipe);
        }
        maze.push(current);
    }

    let mut to_visit = Vec::from([Rc::clone(&maze[start.0][start.1])]);

    while !to_visit.is_empty() {
        let mut new_to_visit = Vec::new();
        for p in to_visit {
            p.borrow_mut().distance = Some(0);

            let pipe = p.borrow();

            pipe.neighbours
                .iter()
                .filter(|p| p.borrow().distance.is_none())
                .for_each(|p| new_to_visit.push(Rc::clone(p)));
        }
        to_visit = new_to_visit;
    }

    // Find S junction type
    let north = if start.0 > 0 {
        match_pipe(
            Rc::clone(&maze[start.0 - 1][start.1]),
            VALID_PIPES.get(&(Direction::North as u8)).unwrap(),
        )
    } else {
        false
    };
    let east = if start.1 + 1 < maze[start.0].len() {
        match_pipe(
            Rc::clone(&maze[start.0][start.1 + 1]),
            VALID_PIPES.get(&(Direction::East as u8)).unwrap(),
        )
    } else {
        false
    };
    let west = if start.1 > 0 {
        match_pipe(
            Rc::clone(&maze[start.0][start.1 - 1]),
            VALID_PIPES.get(&(Direction::West as u8)).unwrap(),
        )
    } else {
        false
    };
    let south = if start.0 + 1 < maze.len() {
        match_pipe(
            Rc::clone(&maze[start.0 + 1][start.1]),
            VALID_PIPES.get(&(Direction::South as u8)).unwrap(),
        )
    } else {
        false
    };

    maze[start.0][start.1].borrow_mut().kind = if north && south {
        PipeType::NS
    } else if north && east {
        PipeType::NE
    } else if north && west {
        PipeType::NW
    } else if south && east {
        PipeType::SE
    } else if south && west {
        PipeType::SW
    } else if east && west {
        PipeType::WE
    } else {
        unreachable!()
    };

    let mut horizontal_tiles = HashSet::new();

    for (i, row) in maze.iter().enumerate() {
        let mut inside_loop = false;
        let mut junction = None;
        for j in 0..row.len() {
            let tile = &maze[i][j];

            let borrowed = tile.borrow();
            if borrowed.distance.is_some() {
                if match_pipe(
                    Rc::clone(tile),
                    &[
                        PipeType::NE,
                        PipeType::SE,
                        PipeType::NW,
                        PipeType::SW,
                        PipeType::NS,
                    ],
                ) {
                    if borrowed.kind == PipeType::NS {
                        inside_loop ^= true;
                    } else if let Some(pipe) = junction {
                        if (pipe == PipeType::NE && borrowed.kind == PipeType::SW)
                            || (pipe == PipeType::SW && borrowed.kind == PipeType::NE)
                            || (pipe == PipeType::NW && borrowed.kind == PipeType::SE)
                            || (pipe == PipeType::SE && borrowed.kind == PipeType::NW)
                        {
                            inside_loop ^= true;
                        }

                        junction = None;
                    } else {
                        junction = Some(borrowed.kind);
                    }
                }
            } else if inside_loop {
                horizontal_tiles.insert((i, j));
            }
        }
    }

    println!("{}", horizontal_tiles.len());

    Ok(())
}
