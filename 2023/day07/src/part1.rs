use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Card {
    A,
    K,
    Q,
    J,
    Ten,
    Nine,
    Eight,
    Seven,
    Six,
    Five,
    Four,
    Three,
    Two,
}

impl Card {
    pub fn from_char(char: char) -> Card {
        match char {
            'A' => Card::A,
            'K' => Card::K,
            'Q' => Card::Q,
            'J' => Card::J,
            'T' => Card::Ten,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            _ => unreachable!("Unknown card"),
        }
    }
}

#[derive(Eq, Ord, PartialEq, PartialOrd)]
pub enum Kind {
    FiveKind,
    FourKind,
    FullHouse,
    ThreeKind,
    TwoPair,
    OnePair,
    HighCard,
}

impl Kind {
    pub fn from_cards(cards: &[Card]) -> Kind {
        let cards = cards.iter().fold(HashMap::new(), |mut map, card| {
            map.entry(card).and_modify(|c| *c += 1).or_insert(1);

            map
        });

        match cards.len() {
            1 => Kind::FiveKind,
            2 => {
                let mut iter = cards.values();

                let current = iter.next().unwrap();
                if *current == 4 || *current == 1 {
                    Kind::FourKind
                } else {
                    Kind::FullHouse
                }
            }
            3 => {
                let mut iter = cards.values();

                let mut current = iter.next().unwrap();
                if *current == 1 {
                    current = iter.next().unwrap();
                }

                if *current == 1 || *current == 3 {
                    Kind::ThreeKind
                } else {
                    Kind::TwoPair
                }
            }
            4 => Kind::OnePair,
            5 => Kind::HighCard,
            _ => unreachable!(),
        }
    }
}

#[derive(Eq, Ord, PartialEq, PartialOrd)]
pub struct Hand {
    pub kind: Kind,
    pub cards: Vec<Card>,
    pub bid: usize,
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut hands: Vec<Hand> = input
        .lines()
        .map(|l| {
            let l = l.unwrap();
            let mut iter = l.split(' ');

            let cards: Vec<Card> = iter.next().unwrap().chars().map(Card::from_char).collect();

            let bid = iter.next().unwrap().parse::<usize>().unwrap();

            let kind = Kind::from_cards(&cards);

            Hand { cards, bid, kind }
        })
        .collect();

    hands.sort();

    println!(
        "{}",
        hands
            .iter()
            .rev()
            .enumerate()
            .map(|(rank, card)| card.bid * (rank + 1))
            .sum::<usize>()
    );

    Ok(())
}
