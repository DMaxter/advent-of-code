use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Card {
    A,
    K,
    Q,
    Ten,
    Nine,
    Eight,
    Seven,
    Six,
    Five,
    Four,
    Three,
    Two,
    Joker,
}

impl Card {
    pub fn from_char(char: char) -> Card {
        match char {
            'A' => Card::A,
            'K' => Card::K,
            'Q' => Card::Q,
            'T' => Card::Ten,
            '9' => Card::Nine,
            '8' => Card::Eight,
            '7' => Card::Seven,
            '6' => Card::Six,
            '5' => Card::Five,
            '4' => Card::Four,
            '3' => Card::Three,
            '2' => Card::Two,
            'J' => Card::Joker,
            _ => unreachable!("Unknown card"),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum Kind {
    FiveKind,
    FourKind,
    FullHouse,
    ThreeKind,
    TwoPair,
    OnePair,
    HighCard,
}

impl Kind {
    pub fn from_cards(cards: &[Card]) -> Kind {
        let cards = cards.iter().fold(HashMap::new(), |mut map, card| {
            map.entry(card).and_modify(|c| *c += 1).or_insert(1);

            map
        });

        let joker = cards.get(&Card::Joker);

        let kind = match cards.len() {
            1 => Kind::FiveKind,
            2 => {
                if joker.is_some() {
                    return Kind::FiveKind;
                }

                let mut iter = cards.values();

                let current = iter.next().unwrap();
                if *current == 4 || *current == 1 {
                    Kind::FourKind
                } else {
                    Kind::FullHouse
                }
            }
            3 => {
                // JAAQQ
                // JAAAQ
                // JJAAQ
                // JJJAQ

                let mut iter = cards.values();

                let mut current = iter.next().unwrap();
                if *current == 1 {
                    current = iter.next().unwrap();
                }

                if *current == 1 || *current == 3 {
                    if joker.is_some() {
                        Kind::FourKind
                    } else {
                        Kind::ThreeKind
                    }
                } else {
                    match joker {
                        None => Kind::TwoPair,
                        Some(1) => Kind::FullHouse,
                        Some(2) => Kind::FourKind,
                        _ => unreachable!(),
                    }
                }
            }
            4 if joker.is_some() => Kind::ThreeKind,
            4 if joker.is_none() => Kind::OnePair,
            5 if joker.is_some() => Kind::OnePair,
            5 if joker.is_none() => Kind::HighCard,
            _ => unreachable!(),
        };

        kind
    }
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Hand {
    pub kind: Kind,
    pub cards: Vec<Card>,
    pub bid: usize,
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut hands: Vec<Hand> = input
        .lines()
        .map(|l| {
            let l = l.unwrap();
            let mut iter = l.split(' ');

            let cards: Vec<Card> = iter.next().unwrap().chars().map(Card::from_char).collect();

            let bid = iter.next().unwrap().parse::<usize>().unwrap();

            let kind = Kind::from_cards(&cards);

            Hand { cards, bid, kind }
        })
        .collect();

    hands.sort();

    println!(
        "{}",
        hands
            .iter()
            .rev()
            .enumerate()
            .map(|(rank, card)| card.bid * (rank + 1))
            .sum::<usize>()
    );

    Ok(())
}
