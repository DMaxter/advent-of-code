use std::{
    fs::File,
    io::{BufReader, Lines},
};

pub struct Range(pub isize, pub isize, pub isize);

impl Range {
    pub fn contains(&self, value: isize) -> Option<isize> {
        if value >= self.0 && value < self.1 {
            Some(self.2)
        } else {
            None
        }
    }

    pub fn intersects(&self, start: isize, end: isize) -> bool {
        (start >= self.0 && start < self.1) || (end >= self.0 && end < self.1)
    }
}

pub struct Map {
    pub ranges: Vec<Range>,
}

impl Map {
    pub fn map(&self, value: isize) -> isize {
        if let Some(result) = self.ranges.iter().find_map(|r| r.contains(value)) {
            value + result
        } else {
            value
        }
    }
}

pub fn create_map(lines: &mut Lines<BufReader<File>>) -> std::io::Result<Map> {
    lines.next();

    let mut l = lines.next().unwrap()?;

    let mut ranges: Vec<Range> = Vec::new();

    while !l.is_empty() {
        let nums: Vec<isize> = l.split(' ').map(|n| n.parse::<isize>().unwrap()).collect();

        ranges.push(Range(nums[1], nums[1] + nums[2], nums[0] - nums[1]));

        l = if let Some(line) = lines.next() {
            line?
        } else {
            break;
        };
    }

    Ok(Map { ranges })
}
