use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use day05::create_map;

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines = input.lines();

    let seeds: Vec<isize> = lines
        .next()
        .unwrap()?
        .split(": ")
        .nth(1)
        .unwrap()
        .split(' ')
        .map(|n| n.parse::<isize>().unwrap())
        .collect();

    lines.next();

    let seed_soil = create_map(&mut lines)?;
    let soil_fertilizer = create_map(&mut lines)?;
    let fertilizer_water = create_map(&mut lines)?;
    let water_light = create_map(&mut lines)?;
    let light_temperature = create_map(&mut lines)?;
    let temperature_humidity = create_map(&mut lines)?;
    let humidity_location = create_map(&mut lines)?;

    println!(
        "{}",
        seeds
            .into_iter()
            .map(
                |s| humidity_location.map(temperature_humidity.map(light_temperature.map(
                    water_light.map(fertilizer_water.map(soil_fertilizer.map(seed_soil.map(s))))
                )))
            )
            .min()
            .unwrap()
    );

    Ok(())
}
