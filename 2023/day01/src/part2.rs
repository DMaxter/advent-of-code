use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let mut string = String::new();

                let mut first: Option<u32> = None;
                let mut last: Option<u32> = None;

                for c in l.chars() {
                    if c.is_ascii_digit() {
                        if !string.is_empty() {
                            string.clear();
                        }

                        let dig = c.to_digit(10).unwrap();

                        if first.is_none() {
                            first = Some(dig);
                        }

                        last = Some(dig);
                    } else {
                        string.push(c);

                        let len = string.len();
                        if len >= 3 {
                            for i in 0..(len - 2) {
                                let dig = match &string[i..] {
                                    "one" => 1,
                                    "two" => 2,
                                    "three" => 3,
                                    "four" => 4,
                                    "five" => 5,
                                    "six" => 6,
                                    "seven" => 7,
                                    "eight" => 8,
                                    "nine" => 9,
                                    "zero" => 0,
                                    _ => continue,
                                };

                                if first.is_none() {
                                    first = Some(dig);
                                }

                                last = Some(dig);
                            }
                        }
                    }
                }

                first.unwrap() * 10 + last.unwrap()
            })
            .sum::<u32>()
    );

    Ok(())
}
