use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let iter = l.chars().filter(|c| c.is_ascii_digit());

                let rev = iter.clone().rev();

                iter.last().unwrap().to_digit(10).unwrap()
                    + rev.last().unwrap().to_digit(10).unwrap() * 10
            })
            .sum::<u32>()
    );

    Ok(())
}
