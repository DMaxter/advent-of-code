use itertools::Itertools;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut galaxies: HashMap<usize, Vec<usize>> = HashMap::new();

    let mut i = 0;
    for l in input.lines() {
        let l = l.unwrap();

        let mut row_galaxies = false;

        l.chars()
            .enumerate()
            .filter(|(_, c)| *c == '#')
            .for_each(|(j, _)| {
                galaxies
                    .entry(j)
                    .and_modify(|v| v.push(i))
                    .or_insert(Vec::from(&[i]));

                row_galaxies = true;
            });

        i += if !row_galaxies { 2 } else { 1 };
    }

    let mut expanded_galaxies = HashMap::new();
    let galaxies_columns: Vec<usize> = galaxies.keys().copied().collect();

    let mut shift = 0;
    for j in 0..=*galaxies.keys().max().unwrap() {
        if !galaxies_columns.contains(&j) {
            shift += 1;
        } else {
            expanded_galaxies.insert(j + shift, galaxies.remove(&j));
        }
    }

    println!(
        "{}",
        expanded_galaxies
            .into_iter()
            .flat_map(|(k, v)| {
                v.unwrap()
                    .clone()
                    .iter()
                    .map(move |n| (*n, k))
                    .collect::<Vec<_>>()
            })
            .combinations(2)
            .map(|pair| {
                let a = pair[0];
                let b = pair[1];

                ((b.0 as isize - a.0 as isize).abs() + (b.1 as isize - a.1 as isize).abs()) as usize
            })
            .sum::<usize>()
    );

    Ok(())
}
