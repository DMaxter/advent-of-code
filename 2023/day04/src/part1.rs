use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let mut numbers = l.split(": ").skip(1).take(1).next().unwrap().split(" |");

                let winning: HashSet<usize> = numbers
                    .next()
                    .unwrap()
                    .split(' ')
                    .filter_map(|n| n.parse::<usize>().ok())
                    .collect();

                let play: HashSet<usize> = numbers
                    .next()
                    .unwrap()
                    .split(' ')
                    .filter_map(|n| n.parse::<usize>().ok())
                    .collect();

                let intersection = winning.intersection(&play).count() as u32;

                if intersection == 0 {
                    0
                } else {
                    usize::pow(2, intersection - 1)
                }
            })
            .sum::<usize>()
    );

    Ok(())
}
