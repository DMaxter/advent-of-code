use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut scratchpads: HashMap<usize, usize> = HashMap::new();

    input.lines().enumerate().for_each(|(i, l)| {
        let l = l.unwrap();
        let num_cards = *scratchpads.entry(i).and_modify(|e| *e += 1).or_insert(1);

        let mut numbers = l.split(": ").skip(1).take(1).next().unwrap().split(" |");

        let winning: HashSet<usize> = numbers
            .next()
            .unwrap()
            .split(' ')
            .filter_map(|n| n.parse::<usize>().ok())
            .collect();

        let play: HashSet<usize> = numbers
            .next()
            .unwrap()
            .split(' ')
            .filter_map(|n| n.parse::<usize>().ok())
            .collect();

        let intersection = winning.intersection(&play).count();

        for c in i + 1..i + 1 + intersection {
            let _ = scratchpads
                .entry(c)
                .and_modify(|e| *e += num_cards)
                .or_insert(num_cards);
        }
    });

    println!("{}", scratchpads.values().sum::<usize>());

    Ok(())
}
