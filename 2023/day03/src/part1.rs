use std::{
    cmp::{max, min},
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let matrix = input
        .lines()
        .map(|l| l.unwrap().chars().collect::<Vec<char>>())
        .collect::<Vec<_>>();

    let mut num = String::new();
    let mut start_index = None;
    let max_height = matrix.len() - 1;
    let mut total = 0;

    for i in 0..=max_height {
        let max_width = matrix[i].len() - 1;

        for j in 0..=max_width {
            match matrix[i][j] {
                '0'..='9' => {
                    num.push(matrix[i][j]);

                    if start_index.is_none() {
                        start_index = Some(j as isize - 1);
                    }
                }
                _ if !num.is_empty() => {
                    let start_column = max(start_index.unwrap(), 0) as usize;
                    let end_column = j;
                    let start_row = max(i as isize - 1, 0) as usize;
                    let end_row = min(max_height, i + 1);

                    let mut symbol = false;
                    let mut i = start_row;

                    while !symbol && i <= end_row {
                        for j in start_column..=end_column {
                            match matrix[i][j] {
                                '0'..='9' | '.' => continue,
                                _ => {
                                    symbol = true;
                                    total += num.parse::<usize>().unwrap();

                                    break;
                                }
                            }
                        }

                        i += 1;
                    }

                    start_index = None;
                    num.clear();
                }
                _ => continue,
            }
        }

        if !num.is_empty() {
            let start_column = max(start_index.unwrap(), 0) as usize;
            let end_column = max_width;
            let start_row = max(i as isize - 1, 0) as usize;
            let end_row = min(max_height, i + 1);

            let mut symbol = false;
            let mut i = start_row;

            while !symbol && i <= end_row {
                for j in start_column..=end_column {
                    match matrix[i][j] {
                        '0'..='9' | '.' => continue,
                        _ => {
                            symbol = true;
                            total += num.parse::<usize>().unwrap();

                            break;
                        }
                    }
                }

                i += 1;
            }

            start_index = None;
            num.clear();
        }

        num.clear()
    }

    println!("{:?}", total);

    Ok(())
}
