use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Debug)]
struct Point(usize, usize);

#[derive(Debug)]
struct Area(Point, Point);

impl Area {
    pub fn intersects(&self, other: &Area) -> bool {
        self.0 .0 <= other.1 .0
            && self.1 .0 >= other.0 .0
            && self.0 .1 <= other.1 .1
            && self.1 .1 >= other.0 .1
    }
}

#[derive(Debug)]
struct Number {
    pub value: usize,
    pub position: Area,
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut gears: Vec<Point> = Vec::new();

    let numbers: HashMap<isize, Vec<Number>> = input
        .lines()
        .enumerate()
        .map(|(i, l)| {
            let mut pos = None;
            let mut nums: Vec<Number> = Vec::new();

            let l = l.unwrap();

            for (j, c) in l.chars().enumerate() {
                if c.is_ascii_digit() {
                    if pos.is_none() {
                        pos = Some(Point(i, j))
                    }
                } else {
                    if let Some(position) = pos {
                        nums.push(Number {
                            value: l[position.1..j].parse::<usize>().unwrap(),
                            position: Area(Point(i, position.1), Point(i, j - 1)),
                        });

                        pos = None;
                    }

                    if c == '*' {
                        gears.push(Point(i, j));
                    }
                }
            }

            if let Some(position) = pos {
                nums.push(Number {
                    value: l[position.1..l.len()].parse::<usize>().unwrap(),
                    position: Area(Point(i, position.1), Point(i, l.len() - 1)),
                });
            }

            (i as isize, nums)
        })
        .collect::<HashMap<isize, Vec<Number>>>();

    let mut total = 0;
    let empty = Vec::new();

    for gear in gears {
        let area = Area(
            Point(
                (gear.0 as isize - 1) as usize,
                (gear.1 as isize - 1) as usize,
            ),
            Point(gear.0 + 1, gear.1 + 1),
        );
        let get_intersections = |n: &&Number| area.intersects(&n.position);

        let mut intersections: Vec<&Number> = Vec::new();
        intersections.extend(
            &numbers
                .get(&(area.0 .0 as isize))
                .unwrap_or(&empty)
                .iter()
                .filter(get_intersections)
                .collect::<Vec<_>>(),
        );
        intersections.extend(
            &numbers
                .get(&(gear.0 as isize))
                .unwrap_or(&empty)
                .iter()
                .filter(get_intersections)
                .collect::<Vec<_>>(),
        );
        intersections.extend(
            &numbers
                .get(&(area.1 .0 as isize))
                .unwrap_or(&empty)
                .iter()
                .filter(get_intersections)
                .collect::<Vec<_>>(),
        );

        if intersections.len() >= 2 {
            total += intersections[0].value * intersections[1].value
        }
    }

    println!("{:?}", total);

    Ok(())
}
