use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines = input.lines();

    let parse_num = |n: &str| {
        if n.is_empty() {
            None
        } else {
            Some(n.parse::<usize>().unwrap())
        }
    };

    let times: Vec<usize> = lines
        .next()
        .unwrap()?
        .split(' ')
        .skip(1)
        .filter_map(parse_num)
        .collect();

    let distances: Vec<usize> = lines
        .next()
        .unwrap()?
        .split(' ')
        .skip(1)
        .filter_map(parse_num)
        .collect();

    let mut total = 1;

    for (time, distance) in times.into_iter().zip(distances) {
        let mut i = 0;

        while i * (time - i) <= distance {
            i += 1
        }

        total *= time - (2 * i) + 1
    }

    println!("{}", total);

    Ok(())
}
