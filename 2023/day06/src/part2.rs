use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines = input.lines();

    let time = lines
        .next()
        .unwrap()?
        .split(' ')
        .skip(1)
        .to_owned()
        .fold(String::new(), |a, b| a + b)
        .parse::<usize>()
        .unwrap();

    let distance = lines
        .next()
        .unwrap()?
        .split(' ')
        .skip(1)
        .to_owned()
        .fold(String::new(), |a, b| a + b)
        .parse::<usize>()
        .unwrap();

    let mut i = 0;

    while i * (time - i) <= distance {
        i += 1
    }

    println!("{}", time - (2 * i) + 1);

    Ok(())
}
