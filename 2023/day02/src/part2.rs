use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();
                let cubes = l
                    .split(": ")
                    .skip(1)
                    .take(1)
                    .last()
                    .unwrap()
                    .split("; ")
                    .flat_map(|clue| clue.split(", "))
                    .flat_map(|play| play.split(' '))
                    .collect::<Vec<&str>>();

                let (mut max_red, mut max_green, mut max_blue) = (0, 0, 0);

                for cube in cubes.chunks(2) {
                    let color = cube[1];
                    let num = cube[0].parse::<usize>().unwrap();

                    match color {
                        "red" if num > max_red => max_red = num,
                        "green" if num > max_green => max_green = num,
                        "blue" if num > max_blue => max_blue = num,
                        _ => continue,
                    }
                }

                max_red * max_green * max_blue
            })
            .sum::<usize>()
    );

    Ok(())
}
