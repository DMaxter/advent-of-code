use std::fs::File;
use std::io::{BufRead, BufReader};

const NUM_RED: usize = 12;
const NUM_GREEN: usize = 13;
const NUM_BLUE: usize = 14;

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .filter_map(|l| {
                let l = l.unwrap();
                let mut line = l.split(": ").collect::<Vec<&str>>();

                let cubes = line
                    .pop()
                    .unwrap()
                    .split("; ")
                    .flat_map(|clue| clue.split(", "))
                    .flat_map(|play| play.split(' '))
                    .collect::<Vec<&str>>();

                let game = line
                    .pop()
                    .unwrap()
                    .split(' ')
                    .skip(1)
                    .take(1)
                    .next()
                    .unwrap()
                    .parse::<usize>()
                    .unwrap();

                for cube in cubes.chunks(2) {
                    let color = cube[1];
                    let num = cube[0].parse::<usize>().unwrap();

                    match color {
                        "red" if num > NUM_RED => return None,
                        "green" if num > NUM_GREEN => return None,
                        "blue" if num > NUM_BLUE => return None,
                        _ => continue,
                    }
                }

                Some(game)
            })
            .sum::<usize>()
    );

    Ok(())
}
