use day09::create_jacobian;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn find_previous(numbers: Vec<isize>) -> isize {
    let a = create_jacobian(numbers);
    println!("{:?}", a);
    a.iter().rev().fold(0, |total, e| {
        println!("{0} {1}", e[0], total);
        e[0] - total
    })
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let numbers: Vec<isize> =
                    l.split(' ').map(|n| n.parse::<isize>().unwrap()).collect();

                let a = find_previous(numbers);

                println!("{}", a);
                a
            })
            .sum::<isize>()
    );

    Ok(())
}
