#![feature(iter_map_windows)]
pub fn create_jacobian(initial: Vec<isize>) -> Vec<Vec<isize>> {
    let mut jacobian = Vec::new();

    let mut current = initial;
    while current.iter().any(|e| *e != 0) {
        jacobian.push(current);
        current = jacobian
            .last()
            .unwrap()
            .iter()
            .map_windows(|[a, b]| **b - **a)
            .collect();
    }

    jacobian.push(current);

    jacobian
}
