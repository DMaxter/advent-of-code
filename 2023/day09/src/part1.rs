use day09::create_jacobian;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn find_next(numbers: Vec<isize>) -> isize {
    create_jacobian(numbers)
        .iter()
        .rev()
        .fold(0, |total, e| total + e.last().unwrap())
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let numbers: Vec<isize> =
                    l.split(' ').map(|n| n.parse::<isize>().unwrap()).collect();

                find_next(numbers)
            })
            .sum::<isize>()
    );

    Ok(())
}
