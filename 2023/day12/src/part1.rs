use day12::{filter_valid, generate_row};
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut lines: HashMap<usize, Vec<String>> = HashMap::new();

    println!(
        "{}",
        input
            .lines()
            .map(|l| {
                let l = l.unwrap();

                let line = l.split(' ').collect::<Vec<_>>();

                let row = line[0];
                let damaged: Vec<_> = line[1]
                    .split(',')
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect();

                filter_valid(
                    lines.entry(row.len()).or_insert(generate_row(row.len())),
                    row,
                    &damaged,
                )
            })
            .sum::<usize>()
    );

    Ok(())
}
