use itertools::Itertools;

const CHARS: [char; 2] = ['.', '#'];

pub fn generate_row(len: usize) -> Vec<String> {
    let mut rows: Vec<_> = CHARS.iter().map(|c| c.to_string()).collect();

    for _ in 1..len {
        rows = rows
            .into_iter()
            .cartesian_product(CHARS.iter())
            .map(|(mut s, c)| {
                s.push(*c);
                s
            })
            .collect();
    }

    rows
}

pub fn filter_valid(all: &[String], row: &str, numbers: &[usize]) -> usize {
    all.iter()
        .filter(|r| {
            let mut num = numbers.iter();
            let mut count = 0;
            for (a, b) in r.chars().zip(row.chars()) {
                if b != '?' && a != b {
                    return false;
                } else if a == '#' {
                    count += 1
                } else if a == '.' && count != 0 {
                    let next = num.next();
                    if next.is_none() || count != *next.unwrap() {
                        return false;
                    }

                    count = 0;
                }
            }

            if count > 0 {
                let next = num.next();
                if next.is_none() || count != *next.unwrap() {
                    return false;
                }
            }

            num.next().is_none()
        })
        .count()
}
