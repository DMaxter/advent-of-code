use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let board = input
        .lines()
        .map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
        .collect::<Vec<Vec<char>>>();

    let mut count = 0;

    for i in 0..board.len() {
        for j in 0..board[i].len() {
            if board[i][j] != 'X' {
                continue;
            }

            // Check left-to-right
            if board[i].len() - j > 3
                && board[i][j + 1] == 'M'
                && board[i][j + 2] == 'A'
                && board[i][j + 3] == 'S'
            {
                count += 1;
            }
            // Check right-to-left
            if j >= 3 && board[i][j - 1] == 'M' && board[i][j - 2] == 'A' && board[i][j - 3] == 'S'
            {
                count += 1;
            }
            // Check top-to-bottom
            if board.len() - i > 3
                && board[i + 1][j] == 'M'
                && board[i + 2][j] == 'A'
                && board[i + 3][j] == 'S'
            {
                count += 1;
            }
            // Check bottom-to-top
            if i >= 3 && board[i - 1][j] == 'M' && board[i - 2][j] == 'A' && board[i - 3][j] == 'S'
            {
                count += 1;
            }
            // Check left-to-right and top-to-bottom
            if board[i].len() - j > 3
                && board.len() - i > 3
                && board[i + 1][j + 1] == 'M'
                && board[i + 2][j + 2] == 'A'
                && board[i + 3][j + 3] == 'S'
            {
                count += 1;
            }
            // Check right-to-left and top-to-bottom
            if j >= 3
                && board.len() - i > 3
                && board[i + 1][j - 1] == 'M'
                && board[i + 2][j - 2] == 'A'
                && board[i + 3][j - 3] == 'S'
            {
                count += 1;
            }
            // Check left-to-right and bottom-to-top
            if board[i].len() - j > 3
                && i >= 3
                && board[i - 1][j + 1] == 'M'
                && board[i - 2][j + 2] == 'A'
                && board[i - 3][j + 3] == 'S'
            {
                count += 1;
            }
            // Check right-to-left and bottom-to-top
            if j >= 3
                && i >= 3
                && board[i - 1][j - 1] == 'M'
                && board[i - 2][j - 2] == 'A'
                && board[i - 3][j - 3] == 'S'
            {
                count += 1;
            }
        }
    }

    println!("{}", count);

    Ok(())
}
