use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let board = input
        .lines()
        .map(|l| -> Vec<char> { l.unwrap().chars().collect::<Vec<char>>() })
        .collect::<Vec<Vec<char>>>();

    let mut count = 0;

    for i in 0..board.len() {
        for j in 0..board[i].len() {
            if board[i][j] != 'A' {
                continue;
            }

            if board[i].len() - j > 1
                && board.len() - i > 1
                && j >= 1
                && i >= 1
                && (board[i - 1][j - 1] == 'M' && board[i + 1][j + 1] == 'S'
                    || board[i - 1][j - 1] == 'S' && board[i + 1][j + 1] == 'M')
                && (board[i - 1][j + 1] == 'M' && board[i + 1][j - 1] == 'S'
                    || board[i - 1][j + 1] == 'S' && board[i + 1][j - 1] == 'M')
            {
                count += 1;
            }
        }
    }

    println!("{}", count);

    Ok(())
}
