use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    fs::File,
    io::{BufRead, BufReader},
    rc::Rc,
};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Point(usize, usize);

#[derive(Clone, Debug)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    fn next(self) -> Self {
        match self {
            Direction::Up => Direction::Right,
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
        }
    }
}

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut guard: Point = Point(0, 0);
    let mut direction = Direction::Up;

    let mut columns: HashMap<usize, Vec<Rc<Point>>> = HashMap::new();
    let mut lines: HashMap<usize, Vec<Rc<Point>>> = HashMap::new();

    let len = input
        .lines()
        .enumerate()
        .inspect(|(x, l)| {
            l.as_ref().unwrap().chars().enumerate().for_each(|(y, c)| {
                if c == '^' {
                    guard = Point(*x, y);
                } else if c == '#' {
                    let point = Rc::new(Point(*x, y));

                    if let Entry::Vacant(e) = columns.entry(y) {
                        e.insert(vec![Rc::clone(&point)]);
                    } else {
                        columns.get_mut(&y).unwrap().push(Rc::clone(&point))
                    }

                    if let Entry::Vacant(e) = lines.entry(*x) {
                        e.insert(vec![Rc::clone(&point)]);
                    } else {
                        lines.get_mut(x).unwrap().push(Rc::clone(&point))
                    }
                }
            })
        })
        .count();

    let mut points: HashSet<Point> = HashSet::new();
    points.insert(guard.clone());

    loop {
        match direction {
            Direction::Up => {
                let line = if let Some(obstacles) = columns.get(&guard.1) {
                    obstacles
                        .iter()
                        .filter_map(|o| if o.0 <= guard.0 { Some(o.0) } else { None })
                        .max()
                } else {
                    None
                };

                if let Some(max) = line {
                    points.extend((max + 1..guard.0).map(|e| Point(e, guard.1)));

                    guard = Point(max + 1, guard.1);
                } else {
                    points.extend((0..guard.0).map(|e| Point(e, guard.1)));
                    break;
                }
            }
            Direction::Right => {
                let line = if let Some(obstacles) = lines.get(&guard.0) {
                    obstacles
                        .iter()
                        .filter_map(|o| if o.1 >= guard.1 { Some(o.1) } else { None })
                        .min()
                } else {
                    None
                };

                if let Some(min) = line {
                    points.extend((guard.1..min).map(|e| Point(guard.0, e)));

                    guard = Point(guard.0, min - 1);
                } else {
                    points.extend((guard.1..len).map(|e| Point(guard.0, e)));
                    break;
                }
            }
            Direction::Down => {
                let line = if let Some(obstacles) = columns.get(&guard.1) {
                    obstacles
                        .iter()
                        .filter_map(|o| if o.0 >= guard.0 { Some(o.0) } else { None })
                        .min()
                } else {
                    None
                };

                if let Some(min) = line {
                    points.extend((guard.0..min).map(|e| Point(e, guard.1)));

                    guard = Point(min - 1, guard.1);
                } else {
                    points.extend((guard.1..len).map(|e| Point(e, guard.1)));
                    break;
                }
            }
            Direction::Left => {
                let line = if let Some(obstacles) = lines.get(&guard.0) {
                    obstacles
                        .iter()
                        .filter_map(|o| if o.1 <= guard.1 { Some(o.1) } else { None })
                        .max()
                } else {
                    None
                };

                if let Some(max) = line {
                    points.extend((max + 1..guard.1).map(|e| Point(guard.0, e)));

                    guard = Point(guard.0, max + 1);
                } else {
                    points.extend((0..guard.0).map(|e| Point(guard.0, e)));
                    break;
                }
            }
        }

        direction = direction.next();
    }

    println!("{}", points.len());

    Ok(())
}
