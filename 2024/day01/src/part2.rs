#![feature(binary_heap_into_iter_sorted)]
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut left: Vec<usize> = Vec::new();
    let mut right: HashMap<usize, usize> = HashMap::new();

    input.lines().for_each(|l| {
        let line = l.unwrap();
        let mut line = line.split(" ");

        left.push(line.next().unwrap().parse::<usize>().unwrap());

        *right
            .entry(line.last().unwrap().parse::<usize>().unwrap())
            .or_default() += 1;
    });

    println!(
        "{}",
        left.into_iter()
            .fold(0, |acc, e| acc + (*right.entry(e).or_default() * e))
    );

    Ok(())
}
