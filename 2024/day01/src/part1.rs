#![feature(binary_heap_into_iter_sorted)]
use std::{
    collections::BinaryHeap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    let mut left: BinaryHeap<isize> = BinaryHeap::new();
    let mut right: BinaryHeap<isize> = BinaryHeap::new();

    input.lines().for_each(|l| {
        let line = l.unwrap();
        let mut line = line.split(" ");

        left.push(line.next().unwrap().parse::<isize>().unwrap());
        right.push(line.last().unwrap().parse::<isize>().unwrap());
    });

    println!(
        "{}",
        left.into_iter_sorted()
            .zip(right.into_iter_sorted())
            .fold(0, |acc, (e1, e2)| acc + (e1 - e2).abs())
    );

    Ok(())
}
