use std::fs::read_to_string;

use regex::Regex;

fn main() -> std::io::Result<()> {
    let regex = Regex::new(r"mul\((\d{1,3}),(\d{1,3})\)").unwrap();

    println!(
        "{}",
        read_to_string("input")?
            .split("do()")
            .flat_map(|line| { line.split("don't()").next() })
            .map(|line| {
                regex
                    .captures_iter(line)
                    .map(|c| {
                        c.get(1).unwrap().as_str().parse::<usize>().unwrap()
                            * c.get(2).unwrap().as_str().parse::<usize>().unwrap()
                    })
                    .sum::<usize>()
            })
            .sum::<usize>()
    );

    Ok(())
}
