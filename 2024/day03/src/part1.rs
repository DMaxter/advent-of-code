use std::fs::read_to_string;

use regex::Regex;

fn main() -> std::io::Result<()> {
    let regex = Regex::new(r"mul\((\d{1,3}),(\d{1,3})\)").unwrap();

    let input = read_to_string("input")?;

    println!(
        "{}",
        regex
            .captures_iter(&input)
            .map(|c| {
                c.get(1).unwrap().as_str().parse::<usize>().unwrap()
                    * c.get(2).unwrap().as_str().parse::<usize>().unwrap()
            })
            .sum::<usize>()
    );

    Ok(())
}
