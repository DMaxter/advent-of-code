use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    fs::read_to_string,
};

fn main() -> std::io::Result<()> {
    let input = read_to_string("input")?;

    let mut iter = input.split("\n\n");
    let (rules, pages) = (iter.next().unwrap(), iter.next().unwrap());

    let mut precedence: HashMap<usize, HashSet<usize>> = HashMap::new();

    rules.lines().for_each(|l| {
        let mut nums = l.split("|");

        let n1 = nums.next().unwrap().parse::<usize>().unwrap();
        let n2 = nums.next().unwrap().parse::<usize>().unwrap();

        if let Entry::Vacant(e) = precedence.entry(n1) {
            let mut set = HashSet::new();
            set.insert(n2);

            e.insert(set);
        } else {
            precedence.get_mut(&n1).unwrap().insert(n2);
        }
    });

    let empty = HashSet::new();

    println!(
        "{}",
        pages
            .lines()
            .map(|l| -> usize {
                let mut set = HashSet::new();

                let numbers = l
                    .split(",")
                    .map(|n| n.parse::<usize>().unwrap())
                    .collect::<Vec<usize>>();

                let mut result = Vec::new();
                let mut incorrect = false;

                for num in numbers.iter() {
                    let intersection = set
                        .intersection(precedence.get(num).unwrap_or(&empty))
                        .copied()
                        .collect::<Vec<usize>>();

                    if !intersection.is_empty() {
                        incorrect = true;

                        result.insert(
                            intersection
                                .iter()
                                .map(|e| result.iter().position(|n| n == e).unwrap())
                                .min()
                                .unwrap(),
                            *num,
                        );
                    } else {
                        result.push(*num);
                    }

                    set.insert(*num);
                }

                if incorrect {
                    *result.get(set.len() / 2).unwrap()
                } else {
                    0
                }
            })
            .sum::<usize>()
    );

    Ok(())
}
