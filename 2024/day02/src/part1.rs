#![feature(iter_map_windows)]

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .filter(|l| -> bool {
                let line = l.as_ref().unwrap();

                let mut iter = line
                    .split(" ")
                    .map(|s| s.parse::<isize>().unwrap())
                    .map_windows(|[e1, e2]| -> isize { e1 - e2 });

                iter.clone().all(|e| (1..=3).contains(&e)) || iter.all(|e| (-3..=-1).contains(&e))
            })
            .count()
    );

    Ok(())
}
