#![feature(iter_map_windows)]

use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use itertools::Itertools;

fn main() -> std::io::Result<()> {
    let input = BufReader::new(File::open("input")?);

    println!(
        "{}",
        input
            .lines()
            .filter(|l| -> bool {
                let line = l.as_ref().unwrap();

                let num_iter = line.split(" ").map(|s| s.parse::<isize>().unwrap());

                num_iter
                    .clone()
                    .combinations(num_iter.count() - 1)
                    .any(|list| {
                        let mut iter = list
                            .into_iter()
                            .map_windows(|[e1, e2]| -> isize { e1 - e2 });
                        iter.clone().all(|e| (1..=3).contains(&e))
                            || iter.all(|e| (-3..=-1).contains(&e))
                    })
            })
            .count()
    );

    Ok(())
}
